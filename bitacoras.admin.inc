<?php
// $Id: Bitacoras.admin.inc,v 1.1 2011/01/12 23:14:48 oskarcalvo Exp $

/**
 * @file
 * Bitacoras module settings file.
 *
 * Bitacoras is a tweeting service. This module allows the submission of Drupal
 * content to Bitacoras services, adding a button to add a vote in the Bitacoras
 * views.
 *
 */

/**
 * Bitacoras settings form.
 *
 * Build a form to select the content types to be submitted to Bitacoras, and the
 * view types where the button must be shown.
 */
function bitacoras_node_settings() {
  $form = array();

  $form['bitacoras_node_types'] = array(
    '#type'           => 'checkboxes',
    '#title'          => t('Node Types'),
    '#options'        => node_get_types('names'),
    '#default_value'  => variable_get('bitacoras_node_types', array('story')),
  );

  $location_options = array(
    'full' => t('Full View'),
    'teaser' => t('Teasers'),
  );
  $form['bitacoras_node_location'] = array(
    '#type'           => 'checkboxes',
    '#title'          => t('Locations'),
    '#options'        => $location_options,
    '#default_value'  => variable_get('bitacoras_node_location', array('full')),
  );


  $form['bitacoras_node_weight'] = array(
    '#type'           => 'weight',
    '#title'          => t('Weight'),
    '#default_value'  => variable_get('bitacoras_node_weight', -5),
    '#description'    => t('Heavier weight will sink button to bottom on node view.'),
  );

  return system_settings_form($form);
}

/**
 * Bitacoras settings form.
 *
 * Build a form to select the content types to be submitted to Bitacoras, and the
 * view types where the button must be shown.
 */
function bitacoras_api_settings() {
  $form = array();

  $form['bitacoras_node_types'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Api - key'),
    '#default_value'  => variable_get('bitacoras_api_key', $default),
  );

  return system_settings_form($form);
}
